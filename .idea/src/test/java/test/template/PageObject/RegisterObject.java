package test.template.PageObject;
import org.openqa.selenium.By;
import org.testng.Assert;
import test.template.common.BasePage;
import test.template.dataObjects.AccountInfo;
import test.template.utils.SeleniumWaitUtils;

public class RegisterObject extends BasePage {
    public static By btnSubmit   = By.xpath("//button[@type='submit']");
    public static By verifyName= By.id("txtFirstname-error");
    public static By verifyEmail= By.id("txtEmail-error");
    public static By verifyCEmail= By.id("txtCEmail-error");
    public static By verifyPass= By.id("txtPassword-error");
    public static By verifyCPass= By.id("txtCPassword-error");
    public static By verifyPhone= By.id("txtPhone-error");
    public static By txtName= By.id("txtFirstname");
    public static By txtCEmail= By.id("txtCEmail");
    public static By txtEmail= By.id("txtEmail");
    public static By txtCPass= By.id("txtCPassword");
    public static By txtPass= By.id("txtPassword");
    public static By txtPhone= By.id("txtPhone");
    public static By verifyRegisterSuccess= By.id("thongbao");
    public static String submitBtn = "";


    public  static void registerFunction(String name, String email,String confirmEmail,String Pass,String confirmPass, String Phone) {
        SeleniumWaitUtils.waitForElement(RegisterObject.txtName);
        driver.findElement(RegisterObject.txtName).sendKeys(name);
        driver.findElement(RegisterObject.txtEmail).sendKeys(email);
        driver.findElement(RegisterObject.txtCEmail).sendKeys(confirmEmail);
        driver.findElement(RegisterObject.txtPass).sendKeys(Pass);
        driver.findElement(RegisterObject.txtCPass).sendKeys(confirmPass);
        driver.findElement(RegisterObject.txtPhone).sendKeys(Phone);
        driver.findElement(RegisterObject.btnSubmit).click();

        driver.findElement(By.xpath(submitBtn));
    }

    public static void  verifyResult(By verifyActual, String expectedText){
        SeleniumWaitUtils.waitForElement(RegisterObject.txtName);
        String actual= driver.findElement(verifyActual).getText();
        String expected= expectedText;
        Assert.assertEquals(actual,expected);
    }


}


