package test.template.cases.demo_project;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;
import test.template.common.Config;
import test.template.utils.SeleniumUtils;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CreateTopic extends BaseWebTestCase {

    @BeforeTest
    public void beforeTest(){

        driver().get("http://hitachi2-demo.os.merch8.com/login");
        loginObject.loginHitachi("user", "password");
        SeleniumUtils.takeScreenShot();
    }

    @Test(priority = 1)
    public void TC_01_createSuccessfulTopic() {
        String uuid = UUID.randomUUID().toString();
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.inputTopic(uuid);
        topicObject.clickSubmitTopic();
        topicObject.verifyCreateTopicInList(uuid);
    }

    @Test(priority = 2)
    public void TC_02_checkMaxlengthTopic() {
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.inputTopic("Nhập text vào ô bên dưới để biết được đoạn text của bạn có bao nhiêu ký tự.Nhập text vào ô bên dưới để biết được đoạn text của bạn có bao nhiêu ký tự.1234561");
        topicObject.clickSubmitTopic();
    }

    @Test(priority = 3)
    public void TC_03_checkBlockDuplicatesTopic() {
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.inputTopic("Test topic 01");
        topicObject.clickSubmitTopic();
    }

    @Test(priority = 4)
    public void TC_04_checkInputEmpty() {
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.clickSubmitTopic();
    }

    @Test(priority = 5)
    public void TC_05_checkInputScript() {
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.inputTopic("<script>destroyWebsite();</script>");
        topicObject.clickSubmitTopic();
    }

    @Test(priority = 6)
    public void TC_06_createSuccessfulTopic() {
        String uuid = UUID.randomUUID().toString();
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.inputTopic(uuid);
        topicObject.clickSubmitTopic();
        topicObject.verifyCreateTopicInList("ncascjafjf");
    }

    @Test(priority = 7)
    public void TC_07_createSuccessfulTopic() {
        String uuid = UUID.randomUUID().toString();
        topicObject.clickOnLinkTopic();
        topicObject.clickOnFromTopic();
        topicObject.inputTopic(uuid);
        topicObject.clickSubmitTopic();
        topicObject.verifyCreateTopicInList("ncascjafjf");
    }


}
