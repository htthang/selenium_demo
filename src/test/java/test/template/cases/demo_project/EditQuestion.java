package test.template.cases.demo_project;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import test.template.cases.base.BaseWebTestCase;
import test.template.utils.RandomUtils;
import test.template.utils.SeleniumUtils;

import java.util.UUID;


public class EditQuestion extends BaseWebTestCase {
    @BeforeTest
    public void beforeTest(){
        WebDriverManager.chromedriver().setup();
        driver().get("http://hitachi2-demo.os.merch8.com/login");
        loginObject.loginHitachi("user", "password");
        SeleniumUtils.takeScreenShot();
    }

    @Test()
    public void editQuestion() {
        String questionName = RandomUtils.getAlphaString();
        String questionDescription = RandomUtils.getAlphaString();
        String editedQuestionName = RandomUtils.getAlphaString();
        questionObject.clickOnLinkQuestion();
        questionObject.clickOnFormQuestion();
        questionObject.inputInformationQuestion(questionName, questionDescription, "1");
        questionObject.clickOnSubmitQuestion();
        questionObject.clickEditQuestionButton(questionName);
        questionObject.inputQuestion(editedQuestionName);
        questionObject.clickOnSubmitQuestion();
        questionObject.verifyEditedQuestionSuccess(editedQuestionName);
        deleteObject.questionName(editedQuestionName);
        deleteObject.verifyDeleteInList(editedQuestionName);
    }

}
