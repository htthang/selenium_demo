package test.template.dataObjects;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountInfo {
    private String name;
    private String email;
    private String confirmEmail;
    private String password;
    private String confirmPassword;
    private String phone;

    public AccountInfo() {
        name = "Nancy";
        email = "Nancy@gmail.com";
        confirmEmail = "Nancy@gmail.com";
        password = "yMFZbrrvT8BUd5k";
        confirmPassword = "yMFZbrrvT8BUd5k";
        phone = "12345678";
    }
}
